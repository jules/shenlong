import React from 'react';

import Page from './Page';
import Incident from './Incident';
import { request } from '../util';

export default class Incidents extends React.Component {
  state = {
    incidents: null,
    page: 0,
  };

  componentDidMount() {
    this.fetchIncidents();
  }

  async fetchIncidents() {
    const resp = await request(`/api/incidents/${this.state.page}`);
    this.setState({ incidents: await resp.json() });
  }

  renderIncidents() {
    if (!this.state.incidents) {
      return null;
    }

    return this.state.incidents.map((incident) => (
      <Incident key={incident.id} incident={incident} />
    ));
  }

  render() {
    return (
      <Page>
        <h1>Incidents</h1>
        {this.renderIncidents()}
      </Page>
    );
  }
}
