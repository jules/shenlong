import React from 'react';
import PropTypes from 'prop-types';

import './Incident.css';
import Stage from './Stage';
import ms from 'ms';

const OUTAGE_TYPES = {
  major_outage: 'Major Outage',
  outage: 'Outage',
  partial_outage: 'Partial Outage',
  degraded_service: 'Degraded Service',
};

export default function Incident({ incident }) {
  const {
    content,
    end_date: end,
    start_date: start,
    title,
    type,
    stages,
  } = incident;

  const startDate = new Date(start * 1000);
  const endDate = end ? new Date(end * 1000) : null;
  const tooltip =
    `Started ${startDate.toLocaleString()}` +
    (endDate ? `, ended ${endDate.toLocaleString()}` : '');
  const ago = ms(Date.now() - start * 1000);
  const agoEnd = end ? ms(Date.now() - end * 1000) : null;

  const stageNodes = stages
    .map((stage) => <Stage key={stage.title} stage={stage} />)
    .reverse(); // show newest first

  return (
    <div className="incident">
      <h2>
        {OUTAGE_TYPES[type] || type}: {title}
      </h2>
      <p>{content}</p>
      <footer title={tooltip}>
        Started {ago} ago
        {end ? `, ended ${agoEnd} ago` : null}
      </footer>
      {stageNodes.length ? <div className="stages">{stageNodes}</div> : null}
    </div>
  );
}

Incident.propTypes = {
  incident: PropTypes.shape({
    content: PropTypes.string,
    end_date: PropTypes.number,
    start_date: PropTypes.number,
    title: PropTypes.string,
    type: PropTypes.string,
    stages: PropTypes.array,
  }),
};
