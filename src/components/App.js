import React from 'react';

import { Router } from '@reach/router';

import Dashboard from './Dashboard';
import Incidents from './Incidents';

export default function App() {
  return (
    <Router>
      <Dashboard path="/" />
      <Incidents path="/incidents/" />
    </Router>
  );
}
