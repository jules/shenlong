import React from 'react';
import PropTypes from 'prop-types';

import './Status.css';
import Incident from './Incident';
import Notice from './Notice';

export default function Status({ incident }) {
  const incidentOngoing = incident && incident.ongoing === 1;
  const node = incidentOngoing ? (
    <Incident incident={incident} />
  ) : (
    'All systems operational'
  );

  return <Notice color={incidentOngoing ? 'red' : 'green'}>{node}</Notice>;
}

Status.propTypes = {
  incident: PropTypes.object,
};
